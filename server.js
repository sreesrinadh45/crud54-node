let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let cors = require("cors")
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use('/register',require('./components/register'));
app.use('/login',require('./components/login'));
app.use('/login2',require('./components/login2'));
app.use('/login3',require('./components/login3'));
app.use('/fetch',require('./components/fetch'));
//app.use('/delete',require('./components/delete'));

app.listen(4000,(req,res)=>{
    console.log('server is running');
})





