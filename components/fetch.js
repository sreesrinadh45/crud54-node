let express = require("express");
let app = express();
let mongodb = require("mongodb");
let client = mongodb.MongoClient

let fetch = express.Router().get("/", (req, res) => {
    client.connect("mongodb://localhost:27017/batch54", (err, db) => {
        if (err) {
            throw err;
        }
        else {
            // console.log("fetch called");
            db.collection("Employee").find().toArray((err, array) => {
                if (err) {
                    throw err;
                }
                else {
                    if (array.length > 0) {
                        res.send(array);
                    } else {
                        res.send({ message: "record not found.." });
                    }
                }
            })

        }
    })
})
module.exports = fetch;

